"""
This module contains a function that replaces specific patterns in a text file.
"""

import re

import click


@click.command()
@click.argument("input_file", type=click.File("r", encoding="utf-8"))
@click.argument("output_file", type=click.File("w", encoding="utf-8"), required=False)
def replace_text(input_file, output_file=None):
    """
    Replaces specific patterns in the input file with their corresponding replacements,
    and writes the result to the output file. If an output file is not provided, the
    result is printed to the console.

    :param input_file: A file object representing the input file to read from.
    :type input_file: file object
    :param output_file: A file object representing the output file to write to.
    :type output_file: file object, optional
    :return: None
    """
    text = input_file.read()
    new_text = re.sub(r"»[^»]*«", lambda match: match.group(0).replace("\n", " "), text)
    new_text = re.sub(r"Prof\.", "Professor", new_text)
    new_text = re.sub(r"Nr\.", "Nummer", new_text)

    new_text = new_text.replace("»", "<<")
    new_text = new_text.replace("«", ">>")
    new_text = new_text.replace("<<", "«")
    new_text = new_text.replace(">>", "»")

    if output_file:
        output_file.write(new_text)
    else:
        click.echo(new_text)

    # pylint: disable=E1120
    # pylint: disable=no-value-for-parameter


if __name__ == "__main__":
    replace_text()  # pylint: disable=no-value-for-parameter
