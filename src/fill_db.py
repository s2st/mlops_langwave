"""
This module contains a function that put contents from input_main_language_file
and input_target_language_file into database with file name output_file.
"""

import click
from lingtrain_aligner import (
    preprocessor,
    splitter,
    aligner,
    resolver,
    reader,
    helper,
    vis_helper,
    metrics,
)

language = splitter.get_supported_languages()


@click.command()
@click.argument('input_main_language_file', type=click.File('r', encoding='utf-8'))
@click.argument('main_language', type=click.Choice(language.keys()), default=None)
@click.argument('input_target_language_file', type=click.File('r', encoding='utf-8'))
@click.argument('target_language', type=click.Choice(language.keys()), default=None)
@click.argument('output_file', type=click.Path())
def fill_db(input_main_language_file, main_language, input_target_language_file, target_language, output_file):
    main_text = input_main_language_file.readlines()
    target_text = input_target_language_file.readlines()

    main_text_prepared = preprocessor.mark_paragraphs(main_text)
    target_text_prepared = preprocessor.mark_paragraphs(target_text)

    splitted_from = splitter.split_by_sentences_wrapper(main_text_prepared, main_language)
    splitted_to = splitter.split_by_sentences_wrapper(target_text_prepared, target_language)

    print('main text len:', len(splitted_from))
    print('target text len:', len(splitted_to))

    # for i, t in enumerate(splitted_from):
    #     print(i, t)

    aligner.fill_db(output_file, main_language, target_language, splitted_from, splitted_to)


if __name__ == "__main__":
    fill_db()  # pylint: disable=no-value-for-parameter
